package com.example;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Declarables;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.amqp.rabbit.config.DirectRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import java.util.List;

import static org.springframework.amqp.core.AcknowledgeMode.MANUAL;

@SpringBootApplication
@EnableRabbit
public class Application {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);
        ctx.close();
    }

    @Bean
    public ApplicationRunner runner() {
        return args -> Main();
    }

    private void Main() {
        this.rabbitTemplate.send("myExchange", "binding.key", MessageBuilder
                .withBody("hello world!".getBytes())
                .setContentType("text/plain")
                .build());
        while (true) {
            try {
////                this.rabbitTemplate.send("myExchange","binding.key", MessageBuilder
////                        .withBody("hello world!".getBytes())
////                        .setContentType("text/plain")
////                        .build());
                Thread.sleep(60000L);
            } catch (Throwable e) {
//
            }
        }
    }

    @Bean
    public FooListener fooListener() {
        return new FooListener();
    }

    @Bean
    public DirectRabbitListenerContainerFactory rabbitListenerContainerFactory(final ConnectionFactory connectionFactory) {
        DirectRabbitListenerContainerFactory factory = new DirectRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setAcknowledgeMode(MANUAL);
        factory.setDefaultRequeueRejected(false);
        factory.setPrefetchCount(1);
        return factory;
    }

    public static class FooListener {
        private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

        @RabbitListener(bindings = {
                @QueueBinding(
                        value = @Queue(name = "myQueue", arguments = @Argument(name = "x-dead-letter-exchange", value = "myDlx")),
                        exchange = @Exchange(name = "myExchange", type = "topic"),
                        key = "binding.key"
                ),
                @QueueBinding(
                        value = @Queue(name = "myDlq", arguments = {@Argument(name = "x-dead-letter-exchange", value = "myExchange"), @Argument(name = "x-message-ttl", value = "30000", type = "java.lang.Integer")}),
                        exchange = @Exchange(name = "myDlx", type = "topic"),
                        key = "binding.key"
                )}
        )
        public void handle(final Channel channel, final @Payload String id, final @Header(AmqpHeaders.DELIVERY_TAG) long tag, final @Header(value = "x-death", required = false) List<String> xd) {
            try {
                LOGGER.info("received msg {}", id);
//                Thread.sleep(30000L);
//                channel.basicAck(tag, false);
//                channel.basicReject(tag, false);
            } catch (final Throwable e) {
                LOGGER.error("Error occurred", e);
            }
            throw new RuntimeException();
        }
    }


}
